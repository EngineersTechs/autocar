### Disclaimer

* This project is originally called 
<a href="https://github.com/hamuchiwa/AutoRCCar" >AutoRCCar</a> and is created by <a href="https://github.com/hamuchiwa" >hamuchiwa</a>.
Special thanks for his hard work !

* We only updated the project and improved it for our utilisation.
Feel free to contact us if you have any questions or post a ticket in the issues section.

* Many parts of the code were not working so we edited it to make it run with no troubles.

	
### Dependencies
* Raspberry Pi: 
  - Picamera
  - Python 2.7.13

* Computer:
  - Python 2.7.14
  - Numpy
  - OpenCV 2.4.10.1
  - Pygame
  - PiSerial
  - SciKitLearn 19.0
  - SciPy 

### About

- raspberry_pi/ 
  -	***stream_client.py***: stream video frames in jpeg format to the host computer / this file acts as a client ! It has to be opened after the server or nothing will happen.
  -	***ultrasonic_client.py***: send distance data measured by sensor to the host computer / this file acts as a client ! It has to be opened after the server.

These two files contain the targeted PC IP address and its port.<br />
*Ex : 192.168.1.100:8000*

- arduino/
  -	***rc_keyboard_control.ino***: acts as a interface between rc controller and computer and allows user to send command via USB serial interface.
The *.ino* file contain all the executed actions for each situations.<br />
*Ex : Forward, Forward-Right, Reverse-Left.*

- computer/
  -	cascade_xml/ 
    - trained cascade classifiers *.xml* files that contain the recongnition of the STOP sign and the traffic light. You can import your own *HAAR* cascades files for others objets recognition.

  -	chess_board/ 
    - images for calibration, captured by pi camera. For best results, take pictures with yours and drop them into the folder.

  -	training_data/ 
    - training data for neural network in npz format, they are generated when you execute the *collect_training_data.py* program.

  -	training_images/ 
    - saved video frames during image training data collection stage (optional)

  -	mlp_xml/ 
    - trained neural network parameters in a *.xml* file, they are the from the *.npz* files after the training with *mlp_training.py* program.
  -	***picam_calibration.py***: pi camera calibration, returns camera matrix. The program use all the pictures of the folder *"chess_board/"*. 
  -	***collect_training_data.py***: receive streamed video frames and label frames for later training. When you exec this program, a pygame windows and the camera preview pop.<br />
You are able to control the car with the arrows of your keyboard. When the training is OK for you, press the *"Q"* key (The key to press is *"A"* if you have an AZERTY keyboard).
  -	***mlp_training.py***: neural network training. The program use all of the *.npz* data to contruct the *.xml* file located in *"mlp_xml/*".
  -	***rc_driver.py***: a multithread server program receives video frames and sensor data, and allows RC car drives by itself with stop sign, traffic light detection and front collision avoidance capabilities.<br/>
  The more you train, the better results and accuracy will be.

- test/
  -	***rc_control_test.py***: RC car control with keyboard arrows. The file use COM between the arduino and your PC. The bauds need to be the same.
  -	***stream_server_test.py***: video streaming from Pi to computer
  -	***ultrasonic_server_test.py***: sensor data streaming from Pi to computer.

- Traffic_signal/ 
  - trafic signal sketch contributed by [@geek111](https://github.com/geek1111)


### How to drive
1. **Flash Arduino**: Flash *“rc_keyboard_control.ino”* to Arduino and run *“rc_control_test.py”* to drive the rc car with keyboard (testing purpose)

2. **Pi Camera calibration:** Take multiple chess board images using pi camera at various angles and put them into “chess_board” folder, run *“picam_calibration.py”* and it returns the camera matrix, those parameters will be used in *“rc_driver.py”*

3. **Collect training data and testing data:** First run *“collect_training_data.py”* and then run *“stream_client.py”* on raspberry pi. User presses keyboard to drive the RC car, frames are saved only when there is a key press action. When finished driving, press “q” to exit, data is saved as a npz file. 

4. **Neural network training:** Run *“mlp_training.py”*, depend on the parameters chosen, it will take some time to train. After training, model will be saved in “mlp_xml” folder

5. **Cascade classifiers training (optional):** trained stop sign and traffic light classifiers are included in the "cascade_xml" folder, if you are interested in training your own classifiers, please refer to [OpenCV documentation](http://docs.opencv.org/doc/user_guide/ug_traincascade.html) and [this great tutorial by Thorsten Ball](http://coding-robin.de/2013/07/22/train-your-own-opencv-haar-classifier.html)

6. **Self-driving in action**: First run *“rc_driver.py”* to start the server on the computer and then run *“stream_client.py”* and *“ultrasonic_client.py”* on raspberry pi. 

### Wiring

- Arduino :

	-	The default way to control the car was to jump wire between an arduino and the stock remote controller of the car. We improve the system by bypassing the controller and emit IR light code directly to the car.
	-	The schematic is easier and grabbing the IR code is simple.
	-	You can make your own PCB with 1 / 3 IR leds, an 2n2222 / bc547b, and 2* 220 ohms resistors / 1* 470 ohms resitor.
<br /><br />

<p style="text-align:center;"><img alt="Schematic IR Arduino UNO" src="https://image.noelshack.com/fichiers/2018/05/7/1517781890-untitled-sketch-bb.png" width="30%"  align="middle" /></p>

-	Raspberry Pi :
	
	-	The wiring of the RPI is very easy, you only have to plug the camera and wire the ultrasonic sensor as shown below.
	-	Don't forget to enable camera in raspi-config.
	- 	You have to power the RPI on a powerbank because it is located on the RC car.
<br /><br />

<p align="center"><a class="no-attachment-icon" href="https://image.noelshack.com/fichiers/2018/05/7/1517783351-rpi-bb.png" target="_blank" rel="nofollow noreferrer noopener"><img alt="Schematic Ultrasonic Sensor Raspberry Pi" src="https://image.noelshack.com/fichiers/2018/05/7/1517783351-rpi-bb.png" width="30%" align="middle" class="js-lazy-loaded"></a></p>


-   RcCar :
    -   We removed the original batteries to power the RcCar with the powerbank wich powers the Raspberry Pi.